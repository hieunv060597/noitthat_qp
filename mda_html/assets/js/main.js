(function ($) {
    window.onload = function () {
        $(document).ready(function () {		
            slider_banner();
            slider_logo_partner();
            resize_img_banner();
        });
    };

})(jQuery);


function slider_logo_partner(){
    $('.our-partner .owl-carousel').owlCarousel({
        loop:true,
        margin: 20,
        autoplay:true,
        nav:true,
        navText: [
            "<i class = 'fa fa-angle-left'></i>",
            "<i class = 'fa fa-angle-right'></i>",
          ],
        dots: false,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            576:{
                items:1,
            },
            768:{
                items:2,
            },
            1024:{
                items:3,
            },

            1400:{
                items:5,
            }
        }
   })
}

function slider_banner(){
    $('.banner-others .owl-carousel').owlCarousel({
        loop:true,
        margin: 0,
        autoplay:true,
        nav:true,
        navText: [
            "<i class = 'fa fa-angle-left'></i>",
            "<i class = 'fa fa-angle-right'></i>",
          ],
        dots: true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            576:{
                items:1,
            },
            768:{
                items:1,
            },
            1024:{
                items:1,
            },

            1400:{
                items:1,
            }
        }
   })
}

function resize_img_banner(){
    var banner = $('.banner-others .container');

    if(banner == null){
        return 0;
    }
    else{
        var width = banner.width();
        var height = width / 2.09;
        $('.banner-box .images').height(height);
    }
}

